@ECHO OFF

SET WORK_DIR=%CD:\=/%
SET USER_DIR=%USERPROFILE:\=/%
SET SAVE_GIT_WORK=%USER_DIR%/git_save.txt
SET UNTRACKED_OUT=%USER_DIR%/untracked.txt
SET CHANGED_OUT=%USER_DIR%/changed.txt

FOR /F "tokens=1,2 delims= " %%B IN ('git status -s') DO (
    IF ["%%B"] == ["??"] CALL :UNTRACKED %WORK_DIR%/%%C
    IF ["%%B"] == ["M"] CALL :CHANGED %%C
)

IF NOT EXIST %SAVE_GIT_WORK% TYPE NUL>%SAVE_GIT_WORK%

TYPE %UNTRACKED_OUT:/=\%>>%SAVE_GIT_WORK%
TYPE %CHANGED_OUT:/=\%>>%SAVE_GIT_WORK%

FOR /F "tokens=* delims= " %%F IN (%SAVE_GIT_WORK%) DO (
    Setlocal EnableDelayedExpansion

    SET _TARGET=%USERPROFILE%\GIT_SAVE_%TIME:~0,2%%TIME:~3,2%%TIME:~6,2%%TIME:~9,2%
    IF NOT EXIST !_TARGET! MKDIR !_TARGET!

    SET "_F=%%F"
    SET "_D=%CD%\"
    CALL SET _P=%%_F:!_D!=%%

    FOR /F "tokens=* delims= " %%R IN ('ECHO !_F! ^| FINDSTR /L !_D!') DO SET _R=%%R

    IF ["!_R!"] EQU ["!_F! "] (
        XCOPY !_F! !_TARGET!\!_P! /A /R /Y
    ) ELSE (
        XCOPY !_F! !_TARGET! /A /R /Y
        DEL %%F
    )

)

REM REMOVE TMP FILES
DEL %UNTRACKED_OUT:/=\%
DEL %CHANGED_OUT:/=\%
DEL %SAVE_GIT_WORK:/=\%

GOTO :EXIT

:CHANGED
    IF NOT EXIST %CHANGED_OUT% TYPE NUL>%CHANGED_OUT%
    SET _PATCH=%USER_DIR%/%~nn1.patch
    git diff %1 > %_PATCH%
    ECHO %_PATCH:/=\%>>%CHANGED_OUT%
GOTO :EOF

:UNTRACKED
    IF NOT EXIST %UNTRACKED_OUT% TYPE NUL>%UNTRACKED_OUT%
    SET _UNTRACKET=%1
    ECHO %_UNTRACKET:/=\%>>%UNTRACKED_OUT%
GOTO :EOF

:EXIT
